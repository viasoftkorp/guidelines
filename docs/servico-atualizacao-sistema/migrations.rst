Sistema de Migrations
=====================

A partir da versão 2023.4.0 o sistema contará com um serviço_ auxiliar para gerenciamento do banco de dados do sistema através de execução de comandos SQL. Esses comandos SQL serão expressados em arquivos e são chamados de migrations. As migrations podem ser criação de colunas, UPDATE em tabelas, etc.
Elas são executadas conforme a versão atual do sistema definida pelo parâmetro ``VersaoAtualSistema`` ou conforme a versão fornecida pela requisição. 

| Para a criação e gerenciamento das migrations utilizamos a ferramenta chamada `Goose <https://github.com/pressly/goose>`_.

.. _serviço: https://bitbucket.org/viasoftkorp/korp.atualizacaosistema/src/master/

----

Como instalar o goose
---------------------

* Para instalar o "`Goose <https://github.com/pressly/goose>`_", abra o cmd do Windows e utilize o seguinte comando:

    .. code-block:: powershell

        go install github.com/pressly/goose/v3/cmd/goose@latest

Como criar uma migration
------------------------

#. Clone o repositório_.

#. Navegue até o diretório dentro do serviço, correspondente à versão do sistema onde a migration será criada: ``korp.atualizacaosistema\infrastructures\migrations\{versão}``.

#. Abra o cmd do Windows e gere uma nova migration executando o seguinte comando:

    .. code-block:: powershell

        goose create {nomeMigration} sql

#. Abrir o arquivo gerado e altere a instrução SQL que está entre as tags. Um exemplo é a migração ``20240812173956_tecn_11.sql``.
    
    .. code-block:: sql

        -- +goose Up
        -- +goose StatementBegin
        A SQL da migration deverá ir aqui
        -- +goose StatementEnd

    .. note::

        - O ``{versão}`` é a versão do ERP que deverá ser executado a migration criada.
        - O ``{nomeMigration}`` deverá seguir as nomenclaturas estabelecida no próximo tópico.

#. Abra os Pull Requests para as branchs ``master`` e as respectivas ``releases`` que necessitam da alteração.

.. _repositório: https://bitbucket.org/viasoftkorp/korp.atualizacaosistema/src/master/    

----

Nomenclatura das migrations
---------------------------

* O nome da migration deve corresponder ao identificador da issue que está atendendo, por exemplo, ``ERPK-1595``.
* O goose irá alterara automaticamente os carácteres ``-`` e ``.`` por ``_``, como também sempre deixar o nome do arquivo em minúsculo.

----

Diferenças entre queries Migrations vs arquivo SQL de versão
------------------------------------------------------------

* Utilizamos a instrução ``GO`` para quebrar cada instrução lógica na atualização tradicional do sistema. No sistema de migrations isso não é necessário e elas devem ser removidas.        



Quando criar uma migration
--------------------------

* As migrações deverão ser feitas somente para os casos em que novas SQL precisam ser adicionadas depois do corte da versão, pois a atualização do sistema durante o ciclo de desenvolvimento continua sendo a query SQL de atualização (arquivo de query na pasta ``SQL/Sources/`` do ERP).        