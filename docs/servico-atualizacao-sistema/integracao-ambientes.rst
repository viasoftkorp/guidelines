Integração ambientes de desenvolvimento e Jenkins
======================================================================================

A atualização do sistema para cada versão executará também as migrations. Quando uma nova migration for criada, siga o passo a passo abaixo para validar as alterações em ambiente de desenvolvimento ou no ambiente de testes automatizados no Jenkins do ERP.

----

#. No repositório do serviço `Korp.AtualizacaoSistema <https://bitbucket.org/viasoftkorp/korp.atualizacaosistema/src/master/>`_, crie branchs iniciadas com o nome da issue que está sendo atendida.

    * Exemplo: ERPK-1234, ERPK-1234-master, ERPK-1234-release.
    
#. Faça a implementação de novas migrations, seguindo o passo a passo da documentação: "`Como criar uma migration <https://korp-guidelines.readthedocs.io/en/latest/docs/servico-atualizacao-sistema/migrations.html#>`_".

    * Incluir a alteração em todas versões desejadas. **Exemplo: 2024.2, 2024.3**.

#. Abra os Pull Requests das implementações realizadas no repositório do serviço Korp.AtualizacaoSistema.

    * Necessário abrir Pull Request para as branchs ``master`` e as respectivas ``releases`` do serviço que necessitam da alteração.
    * O Jenkins irá buildar sua implementação, e vai gerar o executável do serviço no caminho: ``192...\Jenkins\AtualizacaoSistema\{Nome do seu branch}\``.
    * O serviço do Korp.AtualizacaoSistema gerado do Pull Request da ``master``, será utilizado em todos seus Pull Requests do ERP, independente de versão.

#. No repositório do ERP, crie o branch contendo o nome da issue que está sendo atendida.

    * Exemplo: bugfix/ERPK-1234-dev

#. Na pasta ``RepositorioTesteUnitario\CriacaoBaseTeste``, execute o CriarBaseTeste.bat.

    * Com isso a base de testes será recriada, e o FlowTest irá utilizar o executável do serviço Korp.AtualizacaoSistema buildado para o seu branch. Isso funcionará, no ambiente de desenvolvimento do programador, e no Pull Request aberto para o repositório do ERP. 

#. Execute o comanda ``SELECT * FROM ATUALIZACAO_SISTEMA_MIGRATIONS_HISTORY`` junto ao banco, a fim de verificar se a migration implementada foi executada. 

    * Exemplo migration ``20240813141025_tecn_11.sql``:  Na tabela deverá conter o registro com o campo ``version_id`` igual a ``20240813141025``. 

#. Desenvolva ou corrija os testes unitários que validem as alterações implementadas.

#. Abra o Pull Request das alterações realizadas no repositório do `ERP <https://bitbucket.org/viasoftkorp/erp/src/develop/>`_.

    * Adicionar no Pull Request do ERP um comentário com o link para o Pull Request do Korp.AtualizacaoSistema, para que a qualidade saiba que seu Pull Request depende do merge de outro repositório.
        
        * Exemplo: ``Atenção!! Este pull request depende do merge prévio do pull request no repositório Korp.AtualizacaoSistema. Link http://...``
    
    * **Atenção**: Os Pull Requests das implementações realizadas no serviço Korp.AtualizacaoSistema, **sempre deverá ser mesclado primeiro**, antes de mesclar o Pull Request do ERP. Caso contrário, o build da ``develop`` ou ``release`` irá usar o serviço de migrations que não contem as migrations implementadas.

#. Para disponibilizar o serviço Korp.AtualizacaoSistema aos clientes, siga as instruções detalhadas na documentação: "`Liberação da Tag do serviço <https://korp-devops.readthedocs.io/en/latest/docs/awx/liberacao_servico.html#liberacao-de-tag-para-korp-atualizacaosistema>`_"

    * **Lembre-se**: Todas as versões que forem liberadas devem passar pela aprovação do **Alexandre** antes de serem disponibilizadas em produção.

.. warning:: 

    - Após mesclar os Pull Requests do repositório do Korp.AtualizacaoSistema, **não será possível realizar alteração** das migrations, **sendo necessário criar uma nova migration** com as correções.
    - Caso o FlowTest não localize o diretório do Korp.AtualizacaoSistema criado pelo Pull Request, será utilizado o caminho padrão configurado no Jenkins ou no arquivo de configuração do FlowTest. Sendo utilizado o branch de destino (``develop`` ou ``release``). Caso ainda assim não encontre, será utilizado o da ``release`` (ultima versão estável).
    - O nome da branch do ERP será formatado para localizar o Pull Request criado para o serviço, onde irá o projeto e número da issue, retirando informações antes do ``/`` e após o segundo ``-``. **Exemplo: bugfix/ERPK-1234-dev que irá resultar em: ERPK-1234**. 
    - Assim que for mesclado o Pull Request das alterações do serviço na ``master``, o Jenkins irá buildar e atualizar o ambiente da qualidade ``QA-DEV``.
    - Quando for mesclado na ``release`` do serviço, os ambientes ``QA-HMLG`` serão atualizados.
    - Quando gerar tag da ``release`` do serviço, os ambientes ``PRD`` nuvem serão atualizados, e durante a noite clientes on-premise serão atualizados.