Orientações para utilização pela qualidade
================================================

Orientações gerais sobre o processo para mesclar os Pull Requests e como funciona o serviço `Korp.AtualizacaoSistema <https://bitbucket.org/viasoftkorp/korp.atualizacaosistema/src/master/>`_ junto aos ambientes.

----

#. Ao analisar um Pull Request para mesclar no ERP, deve verificar se existe Pull Request do mesmo chamado, para o serviço `Korp.AtualizacaoSistema <https://bitbucket.org/viasoftkorp/korp.atualizacaosistema/src/master/>`_.

#. O programador deve adicionar um comentário no Pull Request do ERP, indicando a existência do Pull Request no `Korp.AtualizacaoSistema <https://bitbucket.org/viasoftkorp/korp.atualizacaosistema/src/master/>`_, porém em casos de esquecimento, confire o Korp.AtualizacaoSistema mesmo assim.
    
#. No `Korp.AtualizacaoSistema <https://bitbucket.org/viasoftkorp/korp.atualizacaosistema/src/master/>`_:

    * Ao mesclar na ``master``, os ambientes ``DEV`` serão atualizados.

    * Ao mesclar na ``release``, os ambientes ``HMLG`` serão atualizados.

    * Ao liberar tag (via template AWX `Liberação da Tag do serviço <https://korp-devops.readthedocs.io/en/latest/docs/awx/liberacao_servico.html#liberacao-de-tag-para-korp-atualizacaosistema>`_), os ambientes ``PRD`` nuvem serão atualizados na hora, e de clientes on-premise atualizará na próxima meia noite.
            
        * **Atenção**: Após solicitar a liberação da tag, encaminhar o link para Alexandre, Kewin ou Eduardo realizarem a aprovação.

.. warning::

    - Os Pull Requests do Korp.AtualizacaoSistema, **sempre** deveram ser mesclado antes dos Pull Requests do ERP.
    - Se for mesclado uso de campo novo na ``release`` do ERP, e então liberado Destkop ou tag dos serviços, sem ter liberado a tag do Korp.AtualizacaoSistema que inclui o campo, poderá gerar problemas em nuvem e em clientes.